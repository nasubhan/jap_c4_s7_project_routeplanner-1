// package com.jap.collection;
// package PRO1
public class Resident {

    //declare the variables
    String fullName;
	int socialSecurityNumber;
	char gender;



    public int getSocialSecurityNumber() {
		return socialSecurityNumber;
	}



	public void setSocialSecurityNumber(int socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}



	public char getGender() {
		return gender;
	}



	public void setGender(char gender) {
		this.gender = gender;
	}

	public String getFullName() {
        return fullName;
    }

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}



	//constructor

	public Resident()
	{
		
	}

	public Resident(String fullName, int socialSecurityNumber, char gender) {
		super();
		this.fullName = fullName;
		this.socialSecurityNumber = socialSecurityNumber;
		this.gender = gender;
	}



	@Override
	public String toString() {
		return "Resident [fullName=" + fullName + ", socialSecurityNumber=" + socialSecurityNumber + ", gender="
				+ gender + "]";
	}
    

}
