// package com.jap.collection;

// package com.jap.collection;
import java.util.*;

public class ResidentService {

    private static List<Resident> residentsRepository = new ArrayList<>();

    public ResidentService(){

    }

    /**
     * create list of all the residents.
     */
    public boolean addResident(Resident resident) {
		residentsRepository.add(resident);
        return true;
    }

   //Search for people with their social security number.

    public Resident searchResident(int socialSecurityNumber){
		Resident res = new Resident();
		for(Resident R : residentsRepository)
		{
			if(R.getSocialSecurityNumber() == socialSecurityNumber)
			{
				res = R;
			}
		}
		return res;
    }

   // Sort the name of the residents in alphabetical order.

    public List getAllNamesSorted(List residentsRepository){
		sort(residentsRepository);
        return residentsRepository;

    }
 public static void sort(List<Resident> list)
    {
  
        list.sort((o1, o2)-> o1.getFullName().compareTo(o2.getFullName()));
    }

   // Fetch the list of residents based on their gender.
    public List<Resident> getAllResidentsByGender(char gender){
		List<Resident> res = new ArrayList<Resident>();
		for(Resident R : residentsRepository)
		{
			if(Character.compare(R.getGender(), gender) == 0)
			{
				res.add(R);
			}
		}

       return res;
    }

}
